import { createApp } from "vue";
import App from "./App.vue";
import { createWebHistory, createRouter } from "vue-router";
import MisDatos from "./components/MisDatos.vue";
import Checkout from "./components/Checkout.vue";
import ThankYou from "./components/ThankYou.vue";


const routes = [
  { path: "/", name: "mis-datos", component: MisDatos },
  { path: "/mis-datos", name: "mis-datos", component: MisDatos },
  { path: "/checkout", name: "checkout", component: Checkout },
  { path: "/thank-you", name: "thank-you", component: ThankYou },
  { path: "/:pathMatch(.*)*", name: "error", component: MisDatos },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// createApp(App)
//   .use(router)
//   .mount("#app");
const app = createApp(App);
app.use(router);
app.mount('#app');