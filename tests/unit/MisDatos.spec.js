import { mount } from '@vue/test-utils'
import MisDatos from '../../src/components/MisDatos.vue';

describe('MisDatos', () => {
    test('Comprobamos que no permite dejar el campo vacío', async () => {
        const wrapper = mount(MisDatos);

        // Comprobamos que no permite input vacío
        wrapper.find('input').element.value='';

        const button = wrapper.find('.btnRounded');
        await button.trigger('click');

        //se muestra el error
        expect(wrapper.find('.error').isVisible()).toBe(true)
    });
    
})



