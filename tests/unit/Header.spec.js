import { mount } from '@vue/test-utils'
import Header from '../../src/components/Header.vue';

describe('Header', () => {
    test('El menú móvil está oculto por defecto', () => {
        const wrapper = mount(Header);
        expect(wrapper.find('.menu').isVisible()).toBe(true)
    });
})



